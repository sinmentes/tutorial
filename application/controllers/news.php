<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class News extends CI_Controller {
	
	public function __construct(){
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');
		$this->load->library('GROCERY_CRUD');
	}
	public function noticias()	{

		$this->grocery_crud->set_table('news');
		$output = $this->grocery_crud->render();

		$this->view($output);
			}
	function view($output = null){
		$this->load->view('news/view',$output);
	}
}
