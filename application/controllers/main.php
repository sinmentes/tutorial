<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');
		$this->load->library('GROCERY_CRUD');

	}

	public function index()
	{
		echo "<h1>Bienvenido al mundo de Codeigniter</h1>";
		die();
	}
	public function employees(){	

		
		$this->grocery_crud->set_table('employees');
		$output = $this->grocery_crud->render();

		$this->_example_output($output);
	}
	function _example_output($output = null){
		$this->load->view('tu_vista',$output);
	}
}